Ansible roles for deployment of additional IT modules to Openstack nodes.
Those roles will be merged to official kolla-ansible playbook to kolla-ansible-srv docker image.


INSTRUCTIONS:
Adding new module (=adding new role in ansible):
- add configuration variables to: 
     - ansible/group_vars/all.yml   (with default value)
     - etc/kolla/globals.yml        (commented, with default value)
- add external or internal passwords to:
     - etc/kolla/passwords.yml      (only name of password variable without value) 
- apply module's role to ansible/destroy.yml for kolla-ansible "destroy" action
- apply module's role to ansible/kolla-host.yml for kolla-ansible "bootstrap-servers" action
- apply module's role to ansible/post-deploy.yml for kolla-ansible "post-deploy" action
- apply module's role to ansible/stop.yml for kolla-ansible "stop" action
- apply module's role to ansible/site.yml for all other kolla-ansible actions (deploy, upgrade, reconfiger, check, precheck)
- add folder to ansible/role with default variables, taske, handlers, templates for module deployment
        /ansible/role/<module_name>/defaults/main.yml       (static variables)
        /ansible/role/<module_name>/handlers/main.yml       (ansible handlers)
        /ansible/role/<module_name>/meta/main.yml           (module's dependencies)
        /ansible/role/<module_name>/tasks/main.yml          (ansible tasks)
        /ansible/role/<module_name>/templates/main.yml      (jinja2 templates)
  Additional files can be added to these folders, which should be included to the main yml.

Not all steps are compulsory. 

